import 'dart:convert';


List userModelFromJson(String str) => List.from(json.decode(str));

String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());

class UserpostModel {
  UserpostModel({
    this.nama,
    this.jenis,
    this.harga,
    this.fotoUrl,
   
  });

  String nama;
  String jenis;
  String harga;
  String fotoUrl;
  

  Map toJson() => {
        "Nama": nama,
        "Jenis": jenis,
        "Harga": harga,
        "FotoUrl": fotoUrl,
      };
}
