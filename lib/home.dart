import 'package:jasa_services/appBar.dart';
import 'package:jasa_services/colorPick.dart';
import 'package:flutter/material.dart';

import 'New/viewmodel.dart';

class BerandaPage extends StatefulWidget {
  BerandaPage({Key key}) : super(key: key);

  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  List dataUser = new List();

  void getDataUser() {
    UserViewModel().getUsers().then((value) {
      setState(() {
        dataUser = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getDataUser();
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(appBar: AllAppBar(), body: _viewList()),
    );
  }

  Widget _viewList() {
    return Container(
      child: dataUser == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
            itemCount: dataUser.length,
            itemBuilder: (context, i) {
              return _produk(dataUser[i]['nama'], dataUser[i]['harga'],
                  dataUser[i]['fotourl']);
            }),
    );
  }

  Widget _produk(String nama, String harga, String url) {
    return new Container(
        color: Warna.grey,
        child: Card(
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Image.network(
                  url,
                  // "https://asset-a.grid.id/crop/0x0:0x0/360x240/photo/2020/04/09/663219154.png",
                  fit: BoxFit.cover,
                  width: 100,
                  height: 100,
                ),
              ),
              Container(
                padding: EdgeInsets.all(5),
                height: 100,
                margin: EdgeInsets.only(left: 8),
                child: Column(
                  children: [
                    Text(
                      nama,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black87,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.attach_file,
                          color: Colors.lightBlueAccent,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Servis",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Warna.biruMuda,
                              backgroundColor: Colors.transparent),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                padding: EdgeInsets.only(left: 20, bottom: 10),
                height: 100,
                child: Text(
                 "Rp. " + harga,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ));
  }
}
