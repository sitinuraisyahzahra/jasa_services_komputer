import 'package:jasa_services/appBar.dart';
import 'package:jasa_services/colorPick.dart';
import 'package:jasa_services/home.dart';
import 'package:jasa_services/models/contact.dart';
import 'package:jasa_services/ui/entryform.dart';
import 'package:jasa_services/ui/viewDaftarProduk.dart';
import 'package:flutter/material.dart';

import 'New/jsonmodel.dart';
import 'New/viewmodel.dart';

class Produk extends StatefulWidget {
  @override
  _ProdukState createState() => _ProdukState();
}

class _ProdukState extends State<Produk> {
  Contact contact;
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(appBar: AllAppBar(), body: _form()),
    );
  }

  Widget _button() {
    return Container(
        child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ViewDaftarProduk()));
            },
            child: Container(
              width: 200,
              padding: EdgeInsets.symmetric(vertical: 15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.grey.shade200,
                        offset: Offset(2, 4),
                        blurRadius: 5,
                        spreadRadius: 2)
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Warna.biruMuda, Warna.biruKeterangan])),
              child: Text(
                'View Produk',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          new InkWell(
            onTap: () {
              navigateToEntryForm(context, contact);
            },
            child: Container(
              width: 200,
              padding: EdgeInsets.symmetric(vertical: 15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.grey.shade200,
                        offset: Offset(2, 4),
                        blurRadius: 5,
                        spreadRadius: 2)
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Warna.biruMuda, Warna.biruKeterangan])),
              child: Text(
                'Tambah Produk',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            ),
          )
        ],
      ),
    ));
  }

  TextEditingController _Nama = new TextEditingController();
  TextEditingController _Jenis = new TextEditingController();
  TextEditingController _Harga = new TextEditingController();
  TextEditingController _FotoUrl = new TextEditingController();
  Widget _form() {
    return new Column(
      children: <Widget>[
        new ListTile(
          leading: const Icon(Icons.paste_rounded),
          title: new TextField(
            controller: _Nama,
            decoration: new InputDecoration(
              hintText: "Nama Barang",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.money),
          title: new TextField(
            controller: _Jenis,
            decoration: new InputDecoration(
              hintText: "Jenis Barang",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.image),
          title: new TextField(
            controller: _Harga,
            decoration: new InputDecoration(
              hintText: "Harga Barang",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.image),
          title: new TextField(
            controller: _FotoUrl,
            decoration: new InputDecoration(
              hintText: "Foto Url",
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        new InkWell(
          onTap: () async {
            UserpostModel commRequest = UserpostModel();
            commRequest.nama = _Nama.text;
            commRequest.jenis = _Jenis.text;
            commRequest.harga = _Harga.text;
            commRequest.fotoUrl = _FotoUrl.text;

            UserViewModel()
                .postUser(userpostModelToJson(commRequest))
                .then((value) => print('success'));
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => BerandaPage()));
          },
          child: Container(
            width: 200,
            padding: EdgeInsets.symmetric(vertical: 15),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey.shade200,
                      offset: Offset(2, 4),
                      blurRadius: 5,
                      spreadRadius: 2)
                ],
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [Warna.orenMuda, Warna.merahKeterangan])),
            child: Text(
              'Tambah Produk',
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          ),
        )
      ],
    );
  }

  Future<Contact> navigateToEntryForm(
      BuildContext context, Contact contact) async {
    var result = await Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) {
      return EntryForm(contact);
    }));
    return result;
  }
}
